pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                checkout scm  
            }
        }

        stage('Validate Commit Message') {
            when {
                branch 'feature/*' 
            }
            steps {
                script {
                    def commitMessage = sh(script: 'git log -1 --pretty=%B', returnStdout: true).trim()
                    
                    if (commitMessage.length() > 50) {
                        error("Commit message is too long. It should be less than 50 characters.")
                    }

                    if (!commitMessage.matches("JIRA-\\d+:.*")) {
                        error("Commit message does not start with a JIRA ticket code (e.g., JIRA-123).")
                    }
                }
            }
        }

        stage('Lint Dockerfiles') {
            steps {
                sh 'docker run --rm -i hadolint/hadolint < Dockerfile'  
            }
        }
    }
}
